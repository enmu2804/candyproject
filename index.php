<?php
require_once 'vendor/smarty/smarty/libs/Smarty.class.php';
$smarty = new Smarty();
$smarty->setTemplateDir("template/");
$smarty->setConfigDir("config");
$smarty->setCacheDir("cache");
$smarty->setCompileDir('templates_c');

$name = 'kien';
$smarty->assign("name", $name);
$smarty->display("template/index.tpl");